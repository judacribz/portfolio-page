$(document).ready(function () {
  var section = '';
  var sectionPath = '/html/about.html';
  var sectionId = '';

  // initial page
  addSection(sectionPath);

  // nav click event to add html to section
  $('li a').not('html').on('click', function () {
    $('li a').removeClass('active');

    section = $(this).attr('id');
    sectionPath = '/html/' + section + '.html';
    sectionId = $('section div').attr('id');

    if ((section + 'Sect') !== sectionId) {
      addSection(sectionPath);
    }
  });

  $('button').on('click', function () {
    section = $('section div').attr('id').slice(0, -4);
    $('li a').removeClass('active');
    $('li a#' + section).addClass('active');
  });

  $('.nav li a').click(function () {
    $('#navbar').collapse('hide');
  });

});


/* Adds html from path to section tags */
function addSection(path) {
  $('#pageContent').hide();

  $.get(path, function (data) {
    $('section').html(data);
  });

  $('#pageContent').fadeIn(250);
}
